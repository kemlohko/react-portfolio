export const projects = [
  {
    title: "DEX",
    subtitle: "JS, Solidity, Truffle, Ganache, Ethereum, Metamask, React and Redux",
    description:
      "I implemented my own ERC20 token (AKK) and an Exchange to trade it. At the end both smart contracts will been running on the Ethereum main network and the Dapp will been hosted in heroku.",
    image: "./project-4.mp4",
    link: "https://bitbucket.org/kemlohko/dex/src/master/",
  },
  {
    title: "Todo App",
    subtitle: "JS, Solidity, Truffle, Ganache and Metamask",
    description:
      "This is my first project in Solidity. I implemented a smart contract to add a new todo to the blockchain or to mark a existing todo as completed. The smart contract run on Ganache.",
    image: "./project-3.mp4",
    link: "https://bitbucket.org/kemlohko/ethereum-todo-list/src/master/",
  },
  {
    title: "Block Explorer",
    subtitle: "JS and local blockchain",
    description:
      "In this project i implemented a block eplorer to search for example for a particular transaction, adress or block in my local blockchain.",
    image: "./project-2.mp4",
    link: "https://bitbucket.org/kemlohko/ethereum-todo-list/src/master/",
  },
  {
    title: "Local blockchain",
    subtitle: "JS, Express and Axios",
    description:
      "I implemented the blockchain data structure and an api to communicate with the blockchain. To create a network we just have to run the api script on different ports and connect them together. ",
    image: "./project-1.mp4",
    link: "https://bitbucket.org/kemlohko/blockchain_javascript/src/master/",
  },
];

export const skills = [
  "JavaScript",
  "Java",
  "Python",
  "Solidity",
  "React",
  "Redux",
  "Truffle",
  "Git"
];
