import { CodeIcon } from "@heroicons/react/solid";
import React from "react";
import { projects } from "../data";


export default function Projects() {
    return (
        <section id="projects" className="text-gray-400 bg-gray-900 body-font">
          <div className="md:container px-5 pb-30 mx-auto text-center lg:px-40">
            <div className="flex flex-col w-full mb-10">
              <CodeIcon className="mx-auto inline-block w-10 mb-4" />
              <h1 className="sm:text-4xl text-3xl font-medium title-font mb-4 text-white">
                Apps I've Built
              </h1>
              <p className="lg:w-2/3 mx-auto leading-relaxed text-base">
                Here you can see some of my recent blockchain projects and you can also visit my bitbucket repository to see the code.
                Feel free to contact me, if you have any questions or sugestions.
              </p>
            </div>
            <div className="flex flex-wrap">

              {projects.map((project) => (
                <div className="w-1/2 px-10 py-10 ">

                    <div className="h-52 pb-10">
                        <a
                          href={project.link}
                          key={project.image}
                        >
                          <h2 className="tracking-widest text-lg title-font font-medium text-green-400 mb-1">
                            {project.title}
                          </h2>
                        </a>
                        <h1 className="title-font text-sm font-medium text-white mb-3">
                          {project.subtitle}
                        </h1>
                        <p className="leading-relaxed">{project.description}</p>
                    </div>

                    <iframe src={project.image} 
                      class="relative w-full h-60" 
                      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                      allowFullScreen
                      frameborder="0" />
                </div> 
              ))}
            </div>
          </div>
        </section>
      );
}