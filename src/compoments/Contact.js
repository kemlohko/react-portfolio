import React from "react";
import { Component } from "react";
const axios = require('axios');

class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contactName: '',
      contactEmail: '',
      message: ''
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange(event) {
    let contactName = this.state.contactName
    let contactEmail = this.state.contactEmail
    let message = this.state.message
    if (event.target.name === 'contactName') contactName = event.target.value
    else if (event.target.name === 'contactEmail') contactEmail = event.target.value
         else if (event.target.name === 'message') message = event.target.value

    this.setState({
      contactName: contactName,
      contactEmail: contactEmail,
      message: message
    })
  }

  handleSubmit(event) {
    event.preventDefault()
    const contactName = this.state.contactName
    const contactEmail = this.state.contactEmail
    const message = this.state.message

     axios.post('/.netlify/functions/send-contact-email', {
      contactName: contactName,
      contactEmail: contactEmail,
      message: message
    }).then(response => {
      this.resetForm()
      window.alert(response.data)
    }).catch(er => console.log(er))
  
  }

  resetForm() {
    this.setState({
      contactName: '',
      contactEmail: '',
      message: ''
    })
  }

  render() {

    return (
      <section id="contact" className="relative">
        <div className="container px-5 py-10 mx-auto flex sm:flex-nowrap flex-wrap">
          <div className="lg:w-2/3 md:w-1/2 bg-gray-900 rounded-lg overflow-hidden sm:mr-10 p-10 flex items-end justify-start relative">
            <iframe
              width="100%"
              height="100%"
              title="map"
              className="absolute inset-0"
              frameBorder={0}
              marginHeight={0}
              marginWidth={0}
              style={{ filter: "opacity(0.7)" }}
              src="https://www.google.com/maps/embed/v1/place?q=marburg&key=AIzaSyBFw0Qbyq9zTFTd-tUY6dZWTgaQzuU17R8"
            />
            <div className="bg-gray-900 absolute flex flex-wrap py-8 px-10 rounded shadow-md">
              <div className="lg:w-1/2 px-0">
                <h2 className="title-font font-semibold text-white tracking-widest text-xs">
                  ADDRESS
                </h2>
                <p className="mt-1">
                  Fuchspaß 36 <br />
                  35039 Marburg
                </p>
              </div>
              <div className="lg:w-1/2 px-6 mt-8 lg:mt-0">
                <h2 className="title-font font-semibold text-white tracking-widest text-xs">
                  EMAIL
                </h2>
                <a className="text-indigo-400 leading-relaxed">
                  alexkemloh@gmail.com
                </a>
                <h2 className="title-font font-semibold text-white tracking-widest text-xs mt-4">
                  PHONE
                </h2>
                <p className="leading-relaxed">+4917680766617</p>
              </div>
            </div>
          </div>
          <form
            netlify
            onSubmit={this.handleSubmit}
            name="contact"
            id="contact"
            className="lg:w-1/3 md:w-1/2 flex flex-col md:ml-auto w-full md:py-8 mt-8 md:mt-0">
            <h2 className="text-white sm:text-4xl text-3xl mb-1 font-medium title-font">
              Hire Me
            </h2>
            <p className="leading-relaxed mb-5">
            I will be very happy to work with you. Do not hesitate to contact me.
            </p>
            <div className="relative mb-4">
              <label htmlFor="name" className="leading-7 text-sm text-gray-400">
                Name
              </label>
              <input
                type="text"
                id="name"
                name="contactName"
                value={this.state.contactName}
                onChange={this.handleChange}
                className="w-full bg-gray-800 rounded border border-gray-700 focus:border-indigo-500 focus:ring-2 focus:ring-indigo-900 text-base outline-none text-gray-100 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
              />
            </div>
            <div className="relative mb-4">
              <label htmlFor="email" className="leading-7 text-sm text-gray-400">
                Email
              </label>
              <input
                type="email"
                id="email"
                name="contactEmail"
                value={this.state.contactEmail}
                onChange={this.handleChange}
                className="w-full bg-gray-800 rounded border border-gray-700 focus:border-indigo-500 focus:ring-2 focus:ring-indigo-900 text-base outline-none text-gray-100 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
              />
            </div>
            <div className="relative mb-4">
              <label
                htmlFor="message"
                className="leading-7 text-sm text-gray-400">
                Message
              </label>
              <textarea
                id="message"
                name="message"
                value={this.state.message}
                onChange={this.handleChange}
                className="w-full bg-gray-800 rounded border border-gray-700 focus:border-indigo-500 focus:ring-2 focus:ring-indigo-900 h-32 text-base outline-none text-gray-100 py-1 px-3 resize-none leading-6 transition-colors duration-200 ease-in-out"
              />
            </div>
            <button
              type="submit"
              className="text-white bg-indigo-500 border-0 py-2 px-6 focus:outline-none hover:bg-indigo-600 rounded text-lg">
              Submit
            </button>
          </form>
        </div>
      </section>
    );
  }
  
}

export default Contact