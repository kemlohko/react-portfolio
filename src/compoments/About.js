import React from "react";

export default function About() {
  return (
    <section id="about">
      <div className="container mx-auto flex px-10 py-20 md:flex-row flex-col items-center">
        <div className="lg:flex-grow md:w-1/2 lg:pr-24 md:pr-16 flex flex-col md:items-start md:text-left mb-16 md:mb-0 items-center text-center">
          <h1 className="title-font sm:text-4xl text-3xl mb-4 font-medium text-white">
            Hi, I'm Alex.
            <br className="hidden lg:inline-block" /> I'm a blockchain and DLT student at the university of applied science mittweida in germany.
          </h1>
          <p className="mb-8 leading-relaxed">
            I'm very passionate with the blockchain technologie, mobile programming, IOT and AI.
            And i'm not afraid to get my hands dirty with new technologies.
          </p>
       
        </div>
        <div >
          <img
            height="200px"
            width="200px"
            className="object-cover object-center rounded"
            alt="hero"
            src="./moi.jpg"
          />
        </div>
      </div>
    </section>
  );
}