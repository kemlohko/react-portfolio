import React from "react";
import Navbar from "./compoments/Navbar";
import About from "./compoments/About";
import Projects from "./compoments/Projects";
import Skills from "./compoments/Skills";
import Contact from "./compoments/Contact";

export default function App() {
  return (
    <main className="text-gray-400 bg-gray-900 body-font">
      <Navbar />
      <About />
      <Projects />
      <Skills />
      <Contact />
    </main>
  );
}
